/*
 * menu.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include <stdio.h>
#include <stdlib.h>

#include "systems.h"
#include "clock.h"
#include "constants.h"

static void display_menu();
static int get_choice();

void menu()
{
	display_menu();
	int choice;
	while ((choice = get_choice()) == ERROR);

	if (choice == QUIT) {
		return;
	}

	struct Systems systems;

	SYSTEMS_prepare_systems(&systems, choice);
	struct Clock* clock = SYSTEMS_get_clock(&systems);
	CLOCK_start_clock(clock);

	SYSTEMS_destroy_systems(&systems);

	return;
}

static void display_menu()
{
	printf("MENU:\n"
			"Select Option:\n"
			"1) Run in manual mode\n"
			"2) Run in automatic mode\n"
			"3) Quit\n"
			"# ");
}

static int get_choice()
{
	int choice;
	scanf("%d", &choice);

	if (choice == 1)
		return RUN_MANUAL;
	if (choice == 2)
		return RUN_AUTOMATIC;
	if (choice == 3)
		return QUIT;
	else
		return ERROR;
}
