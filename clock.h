/*
 * clock.h
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_CLOCK_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_CLOCK_H_

struct Clock
{
	struct Systems* systems;
	int cycles;
};

struct Clock* Clock_prepare_clock(struct Systems* systems);
void CLOCK_start_clock(struct Clock* clock);
void CLOCK_destroy_clock(struct Clock* clock);
int CLOCK_get_number_of_cycles(struct Clock* clock);

#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_CLOCK_H_ */
