/*
 * clock.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include "clock.h"
#include "run_mode.h"
#include "policy.h"
#include "systems.h"
#include "screen.h"

#include <stdlib.h>

struct Clock* Clock_prepare_clock(struct Systems* systems)
{
	struct Clock* clock = malloc(sizeof(struct Clock));
	clock->systems = systems;
	return clock;
}

void CLOCK_start_clock(struct Clock* clock)
{
	int done = 0;

	while (!done) {
		done = RUN_MODE_let_run_mode_run(clock->systems->run_mode);
		POLICY_let_policy_run(clock->systems);
		SCREEN_let_screen_run(clock->systems);

		clock->cycles++;
	}
}

void CLOCK_destroy_clock(struct Clock* clock)
{
	free(clock);
}

int CLOCK_get_number_of_cycles(struct Clock* clock)
{
	return clock->cycles;
}
