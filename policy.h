/*
 * policy.h
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_POLICY_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_POLICY_H_

#include "systems.h"

void POLICY_let_policy_run(struct Systems* systems);

#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_POLICY_H_ */
