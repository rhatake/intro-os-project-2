/*
 * policy.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include "policy.h"
#include "systems.h"
#include "bathroom.h"
#include "constants.h"
#include "BathroomLine.h"
#include "commandupdate.h"

static char check_whos_next(struct Systems* systems);
static int person_is_allowed_to_pass(struct Systems* systems, char person);
static void permit_person_to_pass(struct Systems* systems);

// First come first served policy
void POLICY_let_policy_run(struct Systems* systems)
{
	if (queueCount(systems->bathroom_line->queue) == 0) {
		set_policy_action(systems->command_update, "Nothing");
		return;
	}

	char person = check_whos_next(systems);

	if (person_is_allowed_to_pass(systems, person)) {
		permit_person_to_pass(systems);
		set_policy_action(systems->command_update, "permitting next in line to enter bathroom");
	}
	else {
		set_policy_action(systems->command_update, "Nothing");
	}
}

static char check_whos_next(struct Systems* systems)
{
	char* c;
	queueFront(systems->bathroom_line->queue, (void*) &c);
	return *c;
}

static int person_is_allowed_to_pass(struct Systems* systems, char person)
{
	if ((person == MALE && systems->bathroom->occupying_gender == MALE)
				|| (person == FEMALE && systems->bathroom->occupying_gender == FEMALE)
				|| systems->bathroom->empty == TRUE) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

static void permit_person_to_pass(struct Systems* systems)
{
	systems->bathroom->number_of_persons++;
	char* temp;
	dequeue(systems->bathroom_line->queue, (void*) &temp);
	systems->bathroom->occupying_gender = *temp;
	systems->bathroom->empty = FALSE;
}
