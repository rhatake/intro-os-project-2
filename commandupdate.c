/*
 * commandupdate.c
 *
 *  Created on: Dec 18, 2014
 *      Author: xubuntu
 */


#include "commandupdate.h"
#include <string.h>
#include <stdlib.h>

struct Command_Update* COMMAND_UPDATE_prepare()
{
	struct Command_Update* comup = malloc(sizeof(struct Command_Update));
	comup->run_mode_update = malloc(STR_MAX);
	comup->policy_update = malloc(STR_MAX);
	return comup;
}

void COMMAND_UPDATE_destroy(struct Command_Update* comup)
{
	free(comup->policy_update);
	free(comup->run_mode_update);
	free(comup);
}

void set_policy_action(struct Command_Update* command_update, const char* msg)
{
	bzero(command_update->policy_update, STR_MAX);
	strncpy(command_update->policy_update, msg, STR_MAX);
}

void set_run_mode_action(struct Command_Update* command_update, const char* msg)
{
	bzero(command_update->run_mode_update, STR_MAX);
	strncpy(command_update->run_mode_update, msg, STR_MAX);
}

const char* get_policy_action(struct Command_Update* command_update)
{
	return command_update->policy_update;
}

const char* get_run_mode_action(struct Command_Update* command_update)
{
	return command_update->run_mode_update;
}
