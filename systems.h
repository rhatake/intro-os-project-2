/*
 * systems.h
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_SYSTEMS_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_SYSTEMS_H_

#include "constants.h"

struct Systems
{
	struct Policy* policy;
	struct Bathroom* bathroom;
	struct Clock* clock;
	struct Bathroom_Line* bathroom_line;
	struct Screen* screen;
	struct Run_Mode* run_mode;
	struct Command_Update* command_update;
};

void SYSTEMS_prepare_systems(struct Systems* systems, int run_mode);
struct Clock* SYSTEMS_get_clock(struct Systems* systems);
void SYSTEMS_destroy_systems(struct Systems* systems);

#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_SYSTEMS_H_ */
