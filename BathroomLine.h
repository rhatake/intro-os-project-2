/*
 * BathroomLine.h
 *
 *  Created on: Dec 15, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_BATHROOMLINE_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_BATHROOMLINE_H_

#include "ADT/Queue.h"

struct Bathroom_Line
{
	QUEUE* queue;
	char* people_in_line;
};

struct Bathroom_Line* prepare_bathroom_line();
void destroy_bathroom_line(struct Bathroom_Line*);

void man_wants_to_enter(struct Bathroom_Line* bathroom_line);
void woman_wants_to_enter(struct Bathroom_Line* bathroom_line);
char remove_person_from_line(struct Bathroom_Line* bathroom_line);
int line_is_empty(struct Bathroom_Line* bathroom_line);

const char* get_people_in_line(struct Bathroom_Line* bathroom_line);



#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_BATHROOMLINE_H_ */
