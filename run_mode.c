/*
 * run_mode.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include "run_mode.h"
#include "constants.h"
#include "policy.h"
#include "clock.h"
#include "BathroomLine.h"
#include "bathroom.h"
#include "commandupdate.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static int AUTOMATIC_let_run_mode_run(struct Run_Mode* run_mode);
static int MANUAL_let_run_mode_run(struct Run_Mode* run_mode);
static int get_choice();
static void display_menu();
static int make_choice(float chance);
static void remove_someone_from_bathroom(struct Run_Mode* run_mode);

struct Run_Mode* RUN_MODE_prepare_run_mode(struct Systems* systems, int mode)
{
	struct Run_Mode* run_mode = malloc(sizeof(struct Run_Mode));
	run_mode->systems = systems;
	run_mode->mode = mode;
	return run_mode;
}

int RUN_MODE_let_run_mode_run(struct Run_Mode* run_mode)
{
	if (run_mode->mode == RUN_AUTOMATIC) {
		return AUTOMATIC_let_run_mode_run(run_mode);
	}
	else {
		return MANUAL_let_run_mode_run(run_mode);
	}
}

static int AUTOMATIC_let_run_mode_run(struct Run_Mode* run_mode)
{
	int action_taken = FALSE;

	//randomly decide to add someone to the line or not
	if (make_choice(0.70f) == TRUE && !action_taken) {
		if (make_choice(0.45f) == TRUE && !action_taken) {
			// man to line
			man_wants_to_enter(run_mode->systems->bathroom_line);
			set_run_mode_action(run_mode->systems->command_update, "Man begins to stand in line");
			action_taken = TRUE;
		}
		else {
			// add woman
			woman_wants_to_enter(run_mode->systems->bathroom_line);
			set_run_mode_action(run_mode->systems->command_update, "Woman begins to stand in line");
			action_taken = TRUE;
		}
	}

	// randomly decide on whether to remove someone
	if (make_choice(0.6f) == TRUE && !action_taken) {
		if (!run_mode->systems->bathroom->empty) {
			remove_someone_from_bathroom(run_mode);
			action_taken = TRUE;
		}
	}

	if (!action_taken) {
		set_run_mode_action(run_mode->systems->command_update, "Nothing");
	}

	if (run_mode->systems->clock->cycles == 50) {
		return 1;
	}
	else {
		return 0;
	}
}

static int make_choice(float chance)
{
	return random() < RAND_MAX * chance;
}

static int MANUAL_let_run_mode_run(struct Run_Mode* run_mode)
{
	// show menu
	display_menu();

	int choice;
	while ((choice = get_choice()) == ERROR);

	if (choice == QUIT) {
		return 1;
	}

	// take actions
	if (choice == MALE) {
		man_wants_to_enter(run_mode->systems->bathroom_line);
		set_run_mode_action(run_mode->systems->command_update, "Man begins to stand in line");
	}
	else if (choice == FEMALE) {
		woman_wants_to_enter(run_mode->systems->bathroom_line);
		set_run_mode_action(run_mode->systems->command_update, "woman begins to stand in line");
	}
	else if (choice == REMOVE) {
		if (!run_mode->systems->bathroom->empty) {
			remove_someone_from_bathroom(run_mode);
		}
	}
	else if (choice == NOTHING) {
		set_run_mode_action(run_mode->systems->command_update, "Nothing happened");
		return 0;
	}

	return 0;
}

static void display_menu()
{
	printf("\nMENU\n"
			"Select Option:\n"
			"1) Add Male to queue\n"
			"2) Add Female to queue\n"
			"3) Remove someone from bathroom\n"
			"4) Do nothing\n"
			"5) Quit simulation\n"
			"# ");
}

static int get_choice()
{
	int choice;
	scanf("%d", &choice);

	// some people don't like switch for some reason
	if (choice == 1)
		return MALE;
	if (choice == 2)
		return FEMALE;
	if (choice == 3)
		return REMOVE;
	if (choice == 4)
		return NOTHING;
	if (choice == 5)
		return QUIT;
	else
		return ERROR;
}

static void remove_someone_from_bathroom(struct Run_Mode* run_mode)
{
	if (run_mode->systems->bathroom->occupying_gender == MALE) {
		man_leaves(run_mode->systems->bathroom);
		set_run_mode_action(run_mode->systems->command_update, "Man leaves bathroom");
	}
	else {
		woman_leaves(run_mode->systems->bathroom);
		set_run_mode_action(run_mode->systems->command_update, "Woman leaves bathroom");
	}
}

void RUN_MODE_destroy_run_mode(struct Run_Mode* run_mode)
{
	free(run_mode);
}
