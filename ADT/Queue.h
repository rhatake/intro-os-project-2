#ifndef QUEUE_H_
/*
 * Queue ADT
 * Data Structures a psuedo code approach with C
 * Richard F Gilberg & Behrouz A Forouzan
 */

#define QUEUE_H_

typedef struct node
{
	void* dataPtr;
	struct node* next;
} QUEUE_NODE;

typedef struct
{
	QUEUE_NODE* front;
	QUEUE_NODE* rear;
	int count;
} QUEUE;

typedef int bool;

QUEUE* createQueue(void);
QUEUE* destroyQueue(QUEUE* queue);

bool dequeue(QUEUE* queue, void** itemPtr);
bool enqueue(QUEUE* queue, void* itemPtr);
bool queueFront(QUEUE* queue, void** itemPtr);
bool queueRear(QUEUE* queue, void** itemPtr);
int queueCount(QUEUE* queue);

bool emptyQueue(QUEUE* queue);
bool fullQueue(QUEUE* queue);

#endif /* QUEUE_H_ */
