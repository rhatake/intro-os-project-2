#include "Queue.h"
#include <stdlib.h>

static const int true = 1;
static const int false = 0;

QUEUE* createQueue(void)
{
	QUEUE* queue;

	queue = (QUEUE*) malloc(sizeof(QUEUE));
	if (queue) {
		queue->front = NULL;
		queue->rear = NULL;
		queue->count = 0;
	}
	return queue;
}

QUEUE* destroyQueue(QUEUE* queue)
{
	QUEUE_NODE* deletePtr;

	if (queue) {
		while (queue->front != NULL) {
			// changing this from the book since the author thought it was a good
			// idea to magically free memory behind my back.
			//free(queue->front->dataPtr);
			deletePtr = queue->front;
			queue->front = queue->front->next;
			free(deletePtr);
		}
		free(queue);
	}
	return NULL;
}

bool dequeue(QUEUE* queue, void** itemPtr)
{
	QUEUE_NODE* deleteLoc;

	if (!queue->count) {
		return false;
	}

	*itemPtr = queue->front->dataPtr;
	deleteLoc = queue->front;
	if (queue->count == 1) {
		// deleting only item in queue
		queue->rear = queue->front = NULL;
	}
	else {
		queue->front = queue->front->next;
	}
	(queue->count)--;
	free(deleteLoc);

	return true;
}

bool enqueue(QUEUE* queue, void* itemptr)
{
	QUEUE_NODE* newPtr;

	newPtr = (QUEUE_NODE*) malloc(sizeof(QUEUE_NODE));
	if (!newPtr) {
		return false;
	}

	newPtr->dataPtr = itemptr;
	newPtr->next = NULL;

	if (queue->count == 0) {
		// inserting into null queue
		queue->front = newPtr;
	}
	else {
		queue->rear->next = newPtr;
	}

	(queue->count)++;
	queue->rear = newPtr;
	return true;
}

bool queueFront(QUEUE* queue, void** itemptr)
{
	if (!queue->count) {
		return false;
	}
	else {
		*itemptr = queue->front->dataPtr;
		return true;
	}
}

bool queueRear(QUEUE* queue, void** itemptr)
{
	if (!queue->count) {
		return false;
	}
	else {
		*itemptr = queue->rear->dataPtr;
		return true;
	}
}

int queueCount(QUEUE* queue)
{
	return queue->count;
}

bool emptyQueue(QUEUE* queue)
{
	return (queue->count == 0);
}

bool fullQueue(QUEUE* queue)
{
	QUEUE_NODE* temp;

	temp = (QUEUE_NODE*) malloc(sizeof (*(queue->rear)));
	if (temp) {
		free(temp);
		return true;
	}

	return false;
}
