/*
 * systems.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include "systems.h"
#include "clock.h"
#include "bathroom.h"
#include "screen.h"
#include "run_mode.h"
#include "policy.h"
#include "commandupdate.h"
#include "constants.h"

void SYSTEMS_prepare_systems(struct Systems* systems, int run_mode)
{
	systems->bathroom = BATHROOM_prepare_bathroom();
	systems->bathroom_line = prepare_bathroom_line();
	systems->run_mode = RUN_MODE_prepare_run_mode(systems, run_mode);
	systems->clock = Clock_prepare_clock(systems);
	systems->command_update = COMMAND_UPDATE_prepare();
}

struct Clock* SYSTEMS_get_clock(struct Systems* systems)
{
	return systems->clock;
}

void SYSTEMS_destroy_systems(struct Systems* systems)
{
	CLOCK_destroy_clock(systems->clock);
	RUN_MODE_destroy_run_mode(systems->run_mode);
	destroy_bathroom_line(systems->bathroom_line);
	BATHROOM_destroy_bathroom(systems->bathroom);
}
