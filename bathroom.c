/*
 * bathroom.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include "bathroom.h"
#include "constants.h"
#include <stdlib.h>

struct Bathroom* BATHROOM_prepare_bathroom()
{
	struct Bathroom* bathroom = malloc(sizeof(struct Bathroom));
	bathroom->empty = TRUE;
	bathroom->occupying_gender = 0;
	bathroom->number_of_persons = 0;
	return bathroom;
}
void BATHROOM_destroy_bathroom(struct Bathroom* bathroom)
{
	free(bathroom);
}

void person_leaves(struct Bathroom* bathroom)
{
	if (bathroom->number_of_persons > 0) {
		bathroom->number_of_persons--;
	}

	if (bathroom->number_of_persons == 0) {
		bathroom->empty = TRUE;
	}
}

void man_leaves(struct Bathroom* bathroom)
{
	person_leaves(bathroom);
}

void woman_leaves(struct Bathroom* bathroom)
{
	person_leaves(bathroom);
}
