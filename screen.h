/*
 * screen.h
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_SCREEN_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_SCREEN_H_

#include "bathroom.h"
#include "BathroomLine.h"
#include "systems.h"

void SCREEN_let_screen_run(struct Systems* systems);

#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_SCREEN_H_ */
