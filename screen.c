/*
 * screen.c
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#include "screen.h"
#include "ADT/Queue.h"
#include "bathroom.h"
#include "constants.h"
#include "commandupdate.h"
#include "clock.h"
#include "systems.h"
#include <stdio.h>
#include <stdlib.h>

void SCREEN_let_screen_run(struct Systems* systems)
{
	const char *line_visual = get_people_in_line(systems->bathroom_line);
	line_visual = line_visual == NULL ? "<NO ONE IN LINE>" : line_visual;

	printf("\nLast User Action: %s\n"
			"Last Policy Action: %s\n",
			get_run_mode_action(systems->command_update),
			get_policy_action(systems->command_update));

	printf( "Cycles: %d\n"
			"Bathroom Empty: %s\n"
			"Occupying Gender: %s\n"
			"  # of people: %d\n"
			"People in Line: %d\n"
			"Queue Visualizer, Most Recent Addition First:\n"
			"%s\n\n",
			systems->clock->cycles,
			systems->bathroom->empty == 1 ? "TRUE" : "FALSE",
			systems->bathroom->occupying_gender == MALE ? "MALE" : "FEMALE",
			systems->bathroom->number_of_persons,
			queueCount(systems->bathroom_line->queue),
			line_visual);

}
