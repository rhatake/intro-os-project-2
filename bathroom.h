/*
 * bathroom.h
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_BATHROOM_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_BATHROOM_H_

struct Bathroom
{
	int changed;
	int empty;
	char occupying_gender;
	int number_of_persons;
};

struct Bathroom* BATHROOM_prepare_bathroom();
void BATHROOM_destroy_bathroom(struct Bathroom* bathroom);

void person_leaves(struct Bathroom* bathroom);
void man_leaves(struct Bathroom* bathroom);
void woman_leaves(struct Bathroom* bathroom);


#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_BATHROOM_H_ */
