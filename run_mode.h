/*
 * run_mode.h
 *
 *  Created on: Dec 14, 2014
 *      Author: xubuntu
 */

#ifndef PROJECT_2_BATHROOM_LINE_SIMULATOR_RUN_MODE_H_
#define PROJECT_2_BATHROOM_LINE_SIMULATOR_RUN_MODE_H_

struct Run_Mode
{
	struct Systems* systems;
	int mode;
};

struct Run_Mode* RUN_MODE_prepare_run_mode(struct Systems* systems, int mode);
int RUN_MODE_let_run_mode_run(struct Run_Mode*);
void RUN_MODE_destroy_run_mode(struct Run_Mode*);

#endif /* PROJECT_2_BATHROOM_LINE_SIMULATOR_RUN_MODE_H_ */
