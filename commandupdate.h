/*
 * commandupdate.h
 *
 *  Created on: Dec 18, 2014
 *      Author: xubuntu
 */

#ifndef COMMANDUPDATE_H_
#define COMMANDUPDATE_H_

#define STR_MAX 100

struct Command_Update
{
	char* run_mode_update;
	char* policy_update;
};

struct Command_Update* COMMAND_UPDATE_prepare();
void COMMAND_UPDATE_destroy(struct Command_Update*);

void set_policy_action(struct Command_Update*, const char*);
void set_run_mode_action(struct Command_Update*, const char*);

const char* get_policy_action(struct Command_Update*);
const char* get_run_mode_action(struct Command_Update*);

#endif /* COMMANDUPDATE_H_ */
