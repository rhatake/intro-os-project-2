/*
 * BathroomLine.c
 *
 *  Created on: Dec 15, 2014
 *      Author: xubuntu
 */

#include "BathroomLine.h"
#include "constants.h"
#include <stdlib.h>
#include <strings.h>

static const char male = MALE;
static const char female = FEMALE;

struct Bathroom_Line* prepare_bathroom_line()
{
	struct Bathroom_Line* bathroom_line = (struct Bathroom_Line*) malloc(sizeof(struct Bathroom_Line));
	bathroom_line->queue = createQueue();
	bathroom_line->people_in_line = NULL;
	return bathroom_line;
}

void destroy_bathroom_line(struct Bathroom_Line* bathroom_line)
{
	destroyQueue(bathroom_line->queue);
	if (bathroom_line->people_in_line) {
		free(bathroom_line->people_in_line);
	}
	free(bathroom_line);
}

void man_wants_to_enter(struct Bathroom_Line* bathroom_line)
{
	enqueue(bathroom_line->queue, (void*) &male);
}

void woman_wants_to_enter(struct Bathroom_Line* bathroom_line)
{
	enqueue(bathroom_line->queue, (void*) &female);
}

char remove_person_from_line(struct Bathroom_Line* bathroom_line)
{
	char var;
	dequeue(bathroom_line->queue, (void*) &var);
	return var;
}

int line_is_empty(struct Bathroom_Line* bathroom_line)
{
	return emptyQueue(bathroom_line->queue);
}

const char* get_people_in_line(struct Bathroom_Line* bathroom_line)
{
	if (bathroom_line->people_in_line != 0) {
		free(bathroom_line->people_in_line);
	}

	bathroom_line->people_in_line = (char*) malloc(sizeof(char)*(queueCount(bathroom_line->queue) + 1));
	bzero(bathroom_line->people_in_line, (queueCount(bathroom_line->queue) + 1));

	QUEUE_NODE* i = bathroom_line->queue->front;
	int j = queueCount(bathroom_line->queue) - 1;

	while (TRUE) {
		if (queueCount(bathroom_line->queue) == 0) {
			return NULL;
		}

		bathroom_line->people_in_line[j] = *((char*) i->dataPtr);

		if (i->next == 0) {
			return bathroom_line->people_in_line;
		}

		j--;
		i = i->next;
	}
}
